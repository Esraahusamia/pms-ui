import { Component, OnInit, ViewChild } from '@angular/core';

import {
  DxDataGridModule,
  DxDataGridComponent,
  DxSelectBoxModule,
  DxTextAreaModule,
  DxDateBoxModule,
  DxFormModule,
  DxButtonModule,
  DxFormComponent,
  DxValidatorModule,
  DxValidationSummaryModule
} from 'devextreme-angular';
import { PatientService } from '../../shared/services/patient.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-of-patient-grid',
  templateUrl: './list-of-patient-grid.component.html',
  styleUrls: ['./list-of-patient-grid.component.css'],
})
export class ListOfPatientGridComponent implements OnInit {
  @ViewChild(DxDataGridComponent) grid: DxDataGridComponent;
  @ViewChild(DxFormComponent) form: DxFormComponent;

  /************************************************ Declaration **********************************************/
  customersData: any;
  shippersData: any;
  data;
  url: string;
  dataSource
  EmailPattren: any = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"
  /************************************************ Constructor **********************************************/
  constructor(private PatientService: PatientService, private router: Router, private toastr: ToastrService) {

    // **************************************** GET ALL Patients  ***************************************//
    this.PatientService.GetAllPatients().subscribe(res => {
      console.log(res, "this.UserInfo ")
      this.dataSource = res;
    })

  }
  /************************************************ ngOnInit *********************************************/
  ngOnInit() {
  }


  /***************************************** onRowUpdated function  **************************************/
  onRowUpdated(e) {
    console.log(e, "update")
    this.PatientService.EditUserProfile(e.key, e.data).subscribe(res => {
      console.log(res, "onRowUpdated , response ")
      this.toastr.success('User updated successfully');

    })
  }


  /***************************************** onRowRemoved function **************************************/
  onRowRemoved(e) {
    this.PatientService.DeactivateUserProfile(e.key).subscribe(res => {
      console.log(res, "onRowUpdated , response ")
      this.toastr.success('User removed successfully');

    })
  }

  /***************************************** Show Profile **************************************/

  showProfile(email) {
    this.router.navigate(['patientProfile', email])

  }
}
