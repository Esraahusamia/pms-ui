import { Component, OnInit } from '@angular/core';
import { UserInfo } from '../../shared/models/user-info.model';
import { Patient } from '../../shared/models/patient.model';
import { PatientService } from '../../shared/services/patient.service';
import { trigger, style, transition, animate, keyframes, query, stagger } from '@angular/animations';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { DxDataGridModule } from 'devextreme-angular';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss'],
  animations: [

    trigger('listAnimation', [
      transition('* => *', [

        query(':enter', style({ opacity: 0 }), { optional: true }),

        query(':enter', stagger('300ms', [
          animate('1s ease-in', keyframes([
            style({ opacity: 0, transform: 'translateY(-75%)', offset: 0 }),
            style({ opacity: .5, transform: 'translateY(35px)', offset: 0.3 }),
            style({ opacity: 1, transform: 'translateY(0)', offset: 1.0 }),
          ]))]), { optional: true })
        ,
        query(':leave', stagger('300ms', [
          animate('1s ease-in', keyframes([
            style({ opacity: 1, transform: 'translateY(0)', offset: 0 }),
            style({ opacity: .5, transform: 'translateY(35px)', offset: 0.3 }),
            style({ opacity: 0, transform: 'translateY(-75%)', offset: 1.0 }),
          ]))]), { optional: true })
      ])
    ])

    , trigger('explainerAnim', [
      transition('* => *', [
        query('.col', style({ opacity: 0, transform: 'translateX(-40px)' })),

        query('.col', stagger('500ms', [
          animate('800ms 1.2s ease-out', style({ opacity: 1, transform: 'translateX(0)' })),
        ])),

        query('.col', [
          animate(1000, style('*'))
        ])

      ])
    ])

  ]
})
export class PatientListComponent implements OnInit {
  UserInfo: UserInfo[];
  AllPatient: Patient[];
  errorMessage: string;
  length: number = 0;
  active: boolean
  constructor(private PatientService: PatientService, private router: Router) {
    // **************************************** GET ALL Patients  ***************************************//
    this.PatientService.GetAllPatients().subscribe(res => {
      console.log(res, "this.UserInfo ")
      this.AllPatient = res;
      this.length = this.AllPatient.length, error => this.errorMessage = <any>error;
    })
  }

  ngOnInit() {


  }

  go() {
    this.PatientService.active = true

  }
}
