import { Component, OnInit } from '@angular/core';
import { ChartService } from '../shared/services/chart.service';
import { NgForm } from '@angular/forms';
import { SortEvent } from "../draggable/sortable-list.directive";

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  
  /***********************************************Declearation***********************************************/
  addFieldTabs: boolean = false
  fieldSettingTabs: boolean = true
  guid: number = 1;
  dragElements: any;
  formFields: any;
  current_field: any;
  activeField: any
  chartItems
  constructor(private ChartService: ChartService) {

    this.GetAllChartItems();
  }

  ngOnInit() {
    this.formFields = [];

    this.current_field = {};

    this.activeField = function (f) {
      this.current_field.Active = false;
      this.current_field = f;
      f.Active = true;
      // $("#fieldSettingTab_lnk").tab("show");
    };

    this.formbuilderSortableOpts = {
      "ui-floating": true
    };
  }

  addFieldTab = function () {
    console.log(this.addFieldTabs)
    this.addFieldTabs = false
    this.fieldSettingTabs = true;

  }
  fieldSettingTab = function () {
    this.fieldSettingTabs = false;
    this.addFieldTabs = true

  }
  formbuilderSortableOpts = {
    "ui-floating": true
  };
  changeFieldName = function (Value) {
    this.current_field.Name = Value;
    this.current_field.Settings[0].Value = this.current_field.Name;
    this.current_field.Settings[1].Value = this.current_field.Name;
    this.current_field.Settings[2].Value =
      "x" + this.current_field.Name.replace(/\s/g, "_");
  };
  removeElement = function (idx) {
    console.log(this.formFields, "remove item ", idx, "id")
    if (this.formFields[idx].Active) {
      //  $("#addFieldTab_lnk").tab("show");
      this.current_field = {};
    }
    this.formFields.splice(idx, 1);
  };

  GetAllChartItems = function () {
    this.ChartService.GetAllChartItems().subscribe(res => {
      this.chartItems = res
      this.dragElements = [
        {
          'chartItemID': this.chartItems[2].chartItemID,
          'Name': "Upload Image",
          'Type': this.chartItems[2].chartItemName,
          'Settings': [{
            'Name': 'Field Label',
            'Value': 'Single Text',
            'Type': 'text'
          }, {
            'Name': 'Short Label',
            'Value': 'Single Text',
            'Type': 'text'
          }, {
            'Name': 'Internal Name',
            'Value': 'xSingle_Text',
            'Type': 'text'
          }, {
            'Name': 'Field Type',
            'Value': 'Single Text',
            'Type': 'string'
          }, {
            'Name': 'Single Line Text Options',
            'Value': '',
            'Type': 'label'
          }, {
            'Name': 'Max Input Length',
            'Value': '50',
            'Type': 'text'
          }, {
            'Name': 'Url Template',
            'Value': '',
            'Type': 'text'
          }, {
            'Name': 'Column Span',
            'Value': '2',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2']
          }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': [{
              'Name': 'Required',
              'Value': false
            }, {
              'Name': 'Show on list',
              'Value': false
            }, {
              'Name': 'Unique',
              'Value': false
            }, {
              'Name': 'Index',
              'Value': false
            }]
          }

          ]
        }, {
          'chartItemID': this.chartItems[6].chartItemID,
          'Name': "Single Text",
          'Type': this.chartItems[6].chartItemName,
          'Settings': [{
            'Name': 'Field Label',
            'Value': 'Single Text',
            'Type': 'text'
          }, {
            'Name': 'Short Label',
            'Value': 'Single Text',
            'Type': 'text'
          }, {
            'Name': 'Internal Name',
            'Value': 'xSingle_Text',
            'Type': 'text'
          }, {
            'Name': 'Field Type',
            'Value': 'Single Text',
            'Type': 'string'
          }, {
            'Name': 'Single Line Text Options',
            'Value': '',
            'Type': 'label'
          }, {
            'Name': 'Max Input Length',
            'Value': '50',
            'Type': 'text'
          }, {
            'Name': 'Url Template',
            'Value': '',
            'Type': 'text'
          }, {
            'Name': 'Column Span',
            'Value': '2',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2']
          }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': [{
              'Name': 'Required',
              'Value': false
            }, {
              'Name': 'Show on list',
              'Value': false
            }, {
              'Name': 'Unique',
              'Value': false
            }, {
              'Name': 'Index',
              'Value': false
            }]
          }

          ]
        }, {
          'chartItemID': this.chartItems[7].chartItemID,
          'Name': "Date",
          'Type': this.chartItems[7].chartItemName,
          'Settings': [{
            'Name': 'Field Label',
            'Value': 'Field Label',
            'Type': 'text'
          }, {
            'Name': 'Short Label',
            'Value': 'Short Label',
            'Type': 'text'
          }, {
            'Name': 'Internal Name',
            'Value': 'Internal Name',
            'Type': 'text'
          }, {
            'Name': 'Field Type',
            'Value': 'Date',
            'Type': 'string'
          }, {
            'Name': 'Display Type',
            'Value': '',
            'Type': 'radio',
            'PossibleValue': [
              {
                'Text': 'DateTimeInstance',
                'Checked': true
              },
              {
                'Text': 'DateTimeLocal',
                'Checked': false
              },
              {
                'Text': 'DateLocal',
                'Checked': false
              },
              {
                'Text': 'Time',
                'Checked': false
              },
            ]
          }, {
            'Name': 'Column Span',
            'Value': '2',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2']
          }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': [{
              'Name': 'Required',
              'Value': false
            }, {
              'Name': 'Show on list',
              'Value': false
            }, {
              'Name': 'Unique',
              'Value': false
            }, {
              'Name': 'Index',
              'Value': false
            }]
          }

          ]
        }, {
          'chartItemID': this.chartItems[5].chartItemID,
          'Name': "Pagaraph Text",
          "Type": this.chartItems[5].chartItemName,
          'Settings': [{
            'Name': 'Field Label',
            'Value': '',
            'Type': 'text'
          }, {
            'Name': 'Short Label',
            'Value': '',
            'Type': 'text'
          }, {
            'Name': 'Internal Name',
            'Value': '',
            'Type': 'text'
          }, {
            'Name': 'Field Type',
            'Value': 'Paragraph Text',
            'Type': 'string'
          }, {
            'Name': 'Column Span',
            'Value': '2',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2']
          }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': [{
              'Name': 'Required',
              'Value': false
            }, {
              'Name': 'Enable Rich Text',
              'Value': false
            }, {
              'Name': 'Active',
              'Value': true
            }, {
              'Name': 'Hidden',
              'Value': false
            }]
          }

          ]
        },
        {
          'chartItemID': this.chartItems[8].chartItemID,
          'Name': "Upload File",
          'Type': this.chartItems[8].chartItemName,
          'Settings': [{
            'Name': 'Field Label',
            'Value': 'Single Text',
            'Type': 'text'
          }, {
            'Name': 'Short Label',
            'Value': 'Single Text',
            'Type': 'text'
          }, {
            'Name': 'Internal Name',
            'Value': 'xSingle_Text',
            'Type': 'text'
          }, {
            'Name': 'Field Type',
            'Value': 'Single Text',
            'Type': 'string'
          }, {
            'Name': 'Single Line Text Options',
            'Value': '',
            'Type': 'label'
          }, {
            'Name': 'Max Input Length',
            'Value': '50',
            'Type': 'text'
          }, {
            'Name': 'Url Template',
            'Value': '',
            'Type': 'text'
          }, {
            'Name': 'Column Span',
            'Value': '2',
            'Type': 'dropdown',
            'PossibleValue': ['1', '2']
          }, {
            'Name': 'General Options',
            'Type': 'checkBoxZone',
            'Options': [{
              'Name': 'Required',
              'Value': false
            }, {
              'Name': 'Show on list',
              'Value': false
            }, {
              'Name': 'Unique',
              'Value': false
            }, {
              'Name': 'Index',
              'Value': false
            }]
          }

          ]
        }];

    })
  }
  addElement = function (ele, idx) {
    console.log(ele, "ddddddddddddddddddddd")
    this.current_field.Active = false;

    this.current_field = this.createNewField();
    //Merge setting from template object
    // angular.merge(this.current_field, ele);
    console.log(this.current_field, "  this.current_field")

    Object.assign(this.current_field, ele);

    if (typeof idx == "undefined") {
      this.formFields.push(this.current_field);
    } else {
      this.formFields.splice(idx, 0, this.current_field);
      // $("#fieldSettingTab_lnk").tab("show");
    }
    console.log(this.formFields, "gggggggggggggggg")

  };
  createNewField = function () {
    return {
      id: this.guid++,
      Name: "",
      Settings: [],
      Active: true,
      ChangeFieldSetting: function (Value, SettingName) {
        console.log(Value, "Value")
        console.log(SettingName, "SettingName")
        console.log(this.current_field, "this.current_field")

        switch (SettingName) {
          case "Field Label":
          case "Short Label":
          case "Internal Name":
            this.current_field.Name = Value;
            this.current_field.Settings[0].Value =
              this.current_field.Name;
            this.current_field.Settings[1].Value =
              this.current_field.Name;
            this.current_field.Settings[2].Value =
              "x" + this.current_field.Name.replace(/\s/g, "_");
            break;
          default:
            break;
        }
      },
      GetFieldSetting: function (settingName) {
        var result = {};
        var settings = this.Settings;
        settings.forEach(function (index, set) {
          if (set.Name == settingName) {
            result = set;
            return;
          }
        });
        if (!Object.keys(result).length) {
          //Continue to search settings in the checkbox zone
          settings[settings.length - 1].Options.forEach(function (index, set) {
            if (set.Name == settingName) {
              result = set;
              return;
            }
          });
        }
        return result;
      }
    };
  };


  OnSubmit = function (form: NgForm) {

    console.log(form.value, "fooooooorm value ")
    console.log(this.formFields, "value ")
    var items = [];
    var itemsDetail = { "chartItemID": 0, "Label": "" };
    for (var i = 0; i < this.formFields.length; i++) {
      itemsDetail = { "chartItemID": 0, "Label": "" };
      itemsDetail["chartItemID"] = this.formFields[i].chartItemID;
      itemsDetail["Label"] = this.formFields[i].Name;
      items.push(itemsDetail);
    }
    var tempData = { "chartName": form.value.TemplateName, "Items": items }
    console.log(tempData, "daaaaaaaaaaaaaaaaata")
    this.ChartService.SaveChartItems(tempData).subscribe(res => {
      console.log(res, "chart items response")
    });

  }
  
}
