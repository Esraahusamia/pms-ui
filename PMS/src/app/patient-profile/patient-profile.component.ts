import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../shared/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientService } from '../shared/services/patient.service';
import { ChartService } from '../shared/services/chart.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-patient-profile',
  templateUrl: './patient-profile.component.html',
  styleUrls: ['./patient-profile.component.css']
})
export class PatientProfileComponent implements OnInit {
  /********************************** Declearation ******************************************/

  patientInfo: any;
  patientAddress: any;
  patientArr: any;
  firstName;
  nickName;
  city;
  email;
  /****************************/
  patientEmail: string
  user: boolean = false
  patient: boolean = false;
  staff: boolean = false;
  allAppointment: any;
  upcommingAppointment = [];
  upcommingLength: any
  /****************************/

  /********************************** constructor ******************************************/

  constructor(private http: HttpClient,
    private UserService: UserService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private PatientService: PatientService,
    private ChartService: ChartService) { }

  /********************************** ngOnInit ******************************************/

  ngOnInit() {
    console.log("prooooooofile")
    this.route.params.subscribe(params => {
      this.patientEmail = params['patientEmail']
      this.GetPatientById();
      this.GetAllAppointmentsDate();
    })

    if (JSON.parse(localStorage.getItem("userToken")).role === "Patient") {
      this.patient = true
    }
    else {
      this.staff = true
    }
  }

  /********************************** GetPatientById function ******************************************/

  GetPatientById(): void {
    this.PatientService.GetPatientInfoById(this.patientEmail)
      .subscribe(patientInfo => {
        this.patientInfo = patientInfo[0];
        console.log(this.patientInfo, "this.patientInfo")
        this.PatientService.GetPatientAddressById(this.patientEmail).subscribe(patientAddress => {
          this.patientAddress = patientAddress[0]
          this.user = true
          console.log(this.patientAddress, "this.patientAddress")
        })

      });
  }
  /********************************** GetAllAppointmentsDate function ******************************************/
  GetAllAppointmentsDate(): void {
    this.PatientService.GetAllAppointmentsDate(this.patientEmail).subscribe(res => {
      this.allAppointment = res.length


      console.log("in getUpcomming Function ");

      for (var i = 0; i < res.length; i++) {
        if (new Date(res[i].startDate) > new Date()) {
          this.upcommingAppointment.push(res[i]);
        } else {
          //   if (new Date().getMonth() ) { }
        }
      }
    })
    this.upcommingLength = this.upcommingAppointment.length
  }

  /********************************** Deactivate function ******************************************/

  Deactivate(userEmail) {
    if (confirm('Are you sure to deactivate this account ?') === true) {
      this.toastr.success('User Deactivated successfully');
      this.PatientService.DeactivateUserProfile(userEmail, "Patient")
    }
  }

  // addChart() {
  //   this.router.navigate(['addchart', this.patientEmail])

  // }
  // allChart() {
  //   this.router.navigate(['allCharts', this.patientEmail])

  // }

}
