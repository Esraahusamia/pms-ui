import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, NgForm, FormControl, Validators } from '@angular/forms';
import { NotifierService } from 'angular-notifier';
import { MessageService } from '../../shared/services/message.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { PatientProfile } from '../../shared/models/patient-profile.model';
import { PatientService } from '../../shared/services/patient.service';

@Component({
  selector: 'app-edit-patient-profile',
  templateUrl: './edit-patient-profile.component.html',
  styleUrls: ['./edit-patient-profile.component.css']
})
export class EditPatientProfileComponent implements OnInit {
  private readonly notifier: NotifierService;

  /********************************** Declear Models & form Groups  *********************************/
  accountDetailsForm: FormGroup;
  account_validation_messages: any;
  PatientProfile: PatientProfile;
  PatientEmail;
  PatientInfo: any;
  patient: boolean = false

  /********************************** constructor *************************************************/

  constructor(notifierService: NotifierService,
    private MessageService: MessageService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private PatientService: PatientService) {
    this.notifier = notifierService;

  }

  /********************************** ngOnInit ****************************************************/

  ngOnInit() {

    this.resetForm();
    this.account_validation_messages = this.MessageService.account_validation_messages;
    this.PatientEmail = this.route.parent.url.value[0].path
    this.GetPatientInfoById(this.PatientEmail);
  }
  /********************************** reset Form Function  ******************************************/

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.PatientProfile = {
      UserId: null,
      Discriminator: "",
      UserFirstName: "",
      UserLastName: "",
      UserNickName: "",
      UserGender: "",
      UserDOB: new Date(),
      UserEmail: "",
      UserImageURL: "",
      IsActive: true,
      Password: "",
      confirmPassword: "",
      isAllowed: true,
      HomePhone: null,
      WorkPhone: null,
      FaxPhone: null,
      MobilePhone: null,
      PersonalHealthNumber: null,
      Parent: "",
      FamilyDoctor: "",
      DoctorPhone: null,
      DoctorEmail: "",
      Employer: "",
      ReferingProffessional: "",
      ReferingPhone: null,
      ReferingEmail: "",
      EmergencyContact: "",
      EmergencyPhone: null,
      RelationShip: "",
      Occupation: ""
    }
  }
  /********************************** GetPatientInfoById Function  ******************************************/
  GetPatientInfoById(email) {
    this.PatientService.GetPatientById(email).subscribe(res => {
      this.PatientInfo = res
      this.FormValidation();
    })
  }
  /********************************** FormValidation  Function  ******************************************/

  FormValidation() {
    console.log(this.PatientInfo, "PatientInfo")
    this.patient = true
    this.accountDetailsForm = this.fb.group({
      UserFirstName: new FormControl(this.PatientInfo[0][0].userFirstName, Validators.compose([
        Validators.required
      ])),
      UserLastName: new FormControl(this.PatientInfo[0][0].userLastName, Validators.compose([
        Validators.required
      ])),
      UserNickName: new FormControl(this.PatientInfo[0][0].userNickName, Validators.compose([
        Validators.required
      ])),
      UserId: new FormControl(this.PatientInfo[0][0].userId, Validators.compose([
        Validators.required
      ])),
      UserEmail: new FormControl(this.PatientInfo[0][0].userEmail, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      ])),
      HomePhone: new FormControl(this.PatientInfo[0][0].homePhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      MobilePhone: new FormControl(this.PatientInfo[0][0].mobilePhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      WorkPhone: new FormControl(this.PatientInfo[0][0].workPhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      FaxPhone: new FormControl(this.PatientInfo[0][0].faxPhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      Street: new FormControl(this.PatientInfo[1][0].street, Validators.compose([
        Validators.required
      ])),
      City: new FormControl(this.PatientInfo[1][0].city, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      State: new FormControl(this.PatientInfo[1][0].state, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      Country: new FormControl(this.PatientInfo[1][0].country, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      Postalcode: new FormControl(this.PatientInfo[1][0].postalcode, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{2,5}')
      ])),
      UserDOB: new FormControl(this.PatientInfo[0][0].userDOB, Validators.compose([
        Validators.required
      ])),
      UserGender: new FormControl(this.PatientInfo[0][0].UserGender, Validators.compose([
        Validators.required
      ])),
      PersonalHealthNumber: new FormControl(this.PatientInfo[0][0].personalHealthNumber, Validators.compose([
        Validators.required
      ])),
      FamilyDoctor: new FormControl(this.PatientInfo[0][0].familyDoctor, Validators.compose([
        Validators.required
      ])),
      DoctorPhone: new FormControl(this.PatientInfo[0][0].doctorPhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      DoctorEmail: new FormControl(this.PatientInfo[0][0].doctorEmail, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      ])),
      Parent: new FormControl(this.PatientInfo[0][0].userLastName, Validators.compose([
        Validators.required
      ])),
      Employer: new FormControl(this.PatientInfo[0][0].employer, Validators.compose([
        Validators.required
      ])),
      ReferingProffessional: new FormControl(this.PatientInfo[0][0].referingProffessional, Validators.compose([
        Validators.required
      ])),
      ReferingPhone: new FormControl(this.PatientInfo[0][0].referingPhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      ReferingEmail: new FormControl(this.PatientInfo[0][0].ReferingEmail, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      ])),
      EmergencyContact: new FormControl(this.PatientInfo[0][0].emergencyContact, Validators.compose([
        Validators.required
      ])),
      EmergencyPhone: new FormControl(this.PatientInfo[0][0].emergencyPhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      RelationShip: new FormControl(this.PatientInfo[0][0].relationShip, Validators.compose([
        Validators.required
      ])),
      Occupation: new FormControl(this.PatientInfo[0][0].occupation, Validators.compose([
        Validators.required
      ])),
      Password: new FormControl(this.PatientProfile.Password, Validators.compose([
        Validators.required,
        Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      ]))
    })
  }
}
