export interface AppointmentType {
    LookupDetailsID: number
    LookupID: number
    Title: string
    Description: string
}
