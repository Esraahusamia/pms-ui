import { Time } from "@angular/common";

export interface User {
    Discriminator: string
    UserFirstName: string
    UserLastName: string
    UserNickName: string
    UserGender: string
    UserDOB: Date
    UserMobilePhone: number
    UserEmail: string
    UserImageURL: string
    IsActive: boolean
    Street: string
    City: string
    State: string
    Country: string
    Postalcode: number
    UserAccount: string
    UserStartWorkingHours: Time
    UserEndWorkingHours: Time
    UserStartDate: Date
    Role: string
    UserPassword: string
    UserConfirmPassword: string
    IsAllowedTologin: boolean
}