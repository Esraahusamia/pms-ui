import { Time } from "@angular/common";

export interface UserInfo {
    Discriminator: string
    UserFirstName: string
    UserLastName: string
    UserNickName: string
    UserGender: string
    UserDOB: Date
    UserPhone: number
    UserEmail: string
    UserImageURL: string
    IsActive: boolean
    Password: string
    confirmPassword: string
    role: string
    isAllowed:boolean
    StartDate:Date
    startTime:Time
    endTime:Time

}
