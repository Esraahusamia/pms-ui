import { Injectable } from '@angular/core';
import { Chart } from '../models/chart.model';
import { Observable } from 'rxjs';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class ChartService {
  rootUrl = "http://localhost:53254/NEW/PMS"
  Chart: Chart[];
  constructor(private http: Http) { }


  // **************************************** Get All Chart Items (OBSERVABLE) ***************************************//

  GetAllChartItems(): Observable<Chart[]> {
    return this.http.get(this.rootUrl + '/GetAllChartItems')
      .map((data: Response) => {
        return data.json()
      })
  }

  // **************************************** Save Chart Items  ***************************************//

  SaveChartItems(body) {
    return this.http.post(this.rootUrl + '/AddChartTemplate', body)

  }

  // **************************************** Get ALL Charts Templates**************************************//

  GetAllChartsTemplates(): Observable<Chart[]> {
    return this.http.get(this.rootUrl + '/GetAllChartsTemplates').map((data: Response) => {
      return data.json()
    })

  }

  // **************************************** Get ALL Chart Items By Id  ***************************************//

  GetAllChartItemsById(id) {
    return this.http.get(this.rootUrl + '/GetChartTemplates/' + id).map((data: Response) => {
      return data.json()
    })

  }

  // **************************************** Add Chart Template Values  ***************************************//

  AddChartTemplateValues(body) {
    return this.http.post(this.rootUrl + '/addcharttemplatevalues', body)

  }

  // **************************************** Get ALL Charts  *************************************************//

  GetAllCharts(id) {
    return this.http.get(this.rootUrl + '/GetAllCharts/' + id).map((data: Response) => {
      return data.json()
    })

  }

  // **************************************** Get Chart Value *************************************************//

  GetChartValue(id, visit) {
    return this.http.get(this.rootUrl + '/GetChartValue/' + id + "/" + visit).map((data: Response) => {
      return data.json()
    })

  }

}
