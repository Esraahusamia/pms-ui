import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserInfo } from '../../shared/models/user-info.model'
// import 'rxjs/add/operator/map';
import { map } from "rxjs/operators";
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  patient: any;
  allPatient: UserInfo[];
  rootUrl = "http://localhost:53254/NEW/PMS"
  UserInfo:UserInfo[]
  constructor(private http1: HttpClient , private http: Http) { }

  getPatientInfo(){
    return this.http.get("http://localhost:53254/NEW/PMS/getPersonalInfo/"+'dodo123@gmail.com');
    
  }
  getPatientAddrees(){
    return this.http.get("http://localhost:53254/NEW/PMS/getPatientAddress/" + 'dodo123@gmail.com')
  }
  getAllChart(){
    return this.http.get("http://localhost:53254/NEW/PMS/GetAllCharts/"+"dodo123@gmail.com");
  }
  //send the email to deactivate it
  deactivateProfile(){
    return this.http.post("http://localhost:53254/NEW/PMS/DeactivateUserAccount/" + "userEmail",{})

  }
  
  // **************************************** GET ALL Staff (OBSERVABLE) ***************************************//

  GetStaffById(id): Observable<UserInfo[]> {
    console.log(id,"iiiiid")
    return this.http.post(this.rootUrl + '/GetEmployeeInfo',id)
      .map((data: Response) => {

        return data.json()
      })
  }

}
