import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AbstractControl, FormBuilder, FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import { UserInfo } from '../../shared/models/user-info.model';
import { UserAddress } from '../../shared/models/user-address.model';
import { AuthService } from '../../shared/services/auth.service';
import { MessageService } from '../../shared/services/message.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { PatientService } from '../../shared/services/patient.service';
import { UserService } from '../../shared/services/user.service';

function passwordConfirming(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const pwd = c.parent.get('UserPassword');
  const cpwd = c.parent.get('confirmPassword')

  if (!pwd || !cpwd) return;
  if (pwd.value !== cpwd.value) {
    return { invalid: true };

  }
}
function birthDate(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const UserDOB = c.parent.get('UserDOB');

  if (!UserDOB) return;
  // console.log(new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate(), "test date")
  // console.log(UserDOB.value, "date")

  if (UserDOB.value > new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate()) {
    return { invalid: true };

  }
}
function checkTime(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const startTime = c.parent.get('startTime');
  const endTime = c.parent.get('endTime')

  if (!startTime || !endTime) return;
  if (startTime.value >= endTime.value) {
    return { invalid: true };

  }
}
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  // @Input() staffInfo: any;
  /********************************** Declear Models & form Groups  ******************************************/
  UserInfo: UserInfo;
  UserAddress: UserAddress;
  userRegistrationForm: FormGroup;
  accountDetailsForm: FormGroup;
  staff: boolean = false
  account_validation_messages: any;
  patientInfo: any;
  staffInfo: any;
  patientAddress: any;
  selectedEmployee: string
  Employee: boolean;
  Patient: boolean = true;
  role: any;
  roles: any;
  employeeClicked: boolean
  patientClicked: boolean
  mainPageClicked: boolean
  patientEmail;
  employeeEmail;
  discriminator;
  patientProfile;
  patientForm;
  /********************************** get confirm password to check ******************************************/

  get cpwd() {
    return this.accountDetailsForm.get('confirmPassword');
  }
  get endTime() {
    return this.accountDetailsForm.get('endTime');
  }
  get UserDOB() {
    return this.accountDetailsForm.get('UserDOB');

  }
  /*******************************************constructor*******************************************************/

  constructor(private AuthService: AuthService,
    private MessageService: MessageService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private PatientService: PatientService,
    private UserService: UserService) { }

  /***********************************************************************************************************/

  ngOnInit() {
    /************************************ check the rout params (for refresh)*********************************/

    this.route.params.subscribe(params => {
      if (params['patientEmail']) {
        this.patientEmail = true
      }
      else if (params['employeeEmail']) {
        this.employeeEmail = true;
      }
      else if (params['patientEmailtest']) {
        console.log(params['patientEmailtest'])
        this.patientProfile = true
      }
      else if (params['profileEmail']) {
        console.log(params['profileEmail'], "heeeeere ")
        console.log(this.AuthService.patientClicked, "cliiiiick")

        this.patientForm = true
      }
    })

    this.resetForm();
    this.roles = ["Admin", "Doctor", "Staff", "Assistant", "Patient"]
    this.account_validation_messages = this.MessageService.account_validation_messages;
    this.role = JSON.parse(localStorage.getItem("userToken"))
    /************************ check the role and rout params to show correct form********************************/

    if (!this.role) {
      this.mainPageClicked = true
      this.Employee = false;
      this.FormValidation();
    }
    else if (this.role.role != "Patient" && (this.patientEmail || this.patientProfile)) {

      this.Employee = false;
      this.route.params.subscribe(params => {
        this.patientEmail = params['patientEmail']
        this.GetPatientById();
        console.log(this.patientEmail, "patientEmail")
      })
    }
    else if (this.role.role != "Patient" && (this.AuthService.employeeClicked || this.employeeEmail)) {
      this.Employee = true;
      this.route.params.subscribe(params => {
        this.employeeEmail = params['email']
        this.discriminator = params['id']
        this.GetStaffById();
        console.log(this.employeeEmail, "email")
      })
    }
    else if (this.role && this.role.role != "Patient" && this.AuthService.mainPageClicked || (!this.patientEmail || !this.employeeEmail)) {
      this.mainPageClicked = true
      this.Employee = true;
      this.FormValidation();

    }
    // else if (this.role && this.role.role != "Patient" && this.patientForm && this.patientClicked ) {
    //   console.log(this.patientForm, "paaaaaaaaaaaaaaaaaaaaaaaaaaaaaaatient")
    //   this.Employee = true;
    //   this.route.params.subscribe(params => {
    //     this.patientEmail = params['patientEmail']
    //     this.GetPatientById();
    //     console.log(this.patientEmail, "patientEmail")
    //   })
    // }
    else {
      console.log("elllllllllse")
      this.Employee = false;
      this.FormValidation();

    }

  }

  /********************************** Get Patient By Id Function  ******************************************/

  GetPatientById(): void {
    // const id = this.route.snapshot.paramMap.get('id');

    console.log(this.patientEmail, " ::::::::::::::::::")
    this.PatientService.GetPatientInfoById(this.patientEmail)
      .subscribe(patientInfo => {
        this.patientInfo = patientInfo;
        console.log(this.patientInfo, "this.patientInfo")
        this.PatientService.GetPatientAddressById(this.patientEmail).subscribe(patientAddress => {
          this.patientAddress = patientAddress
          this.FormValidation();

          console.log(this.patientAddress, "this.patientAddress")
        })

      });


    // this.PatientService.GetPatientById(id).subscribe(patient=>{
    //   this.patientInfo= patient[0]
    //   this.patientAddress= patient[1]
    //   console.log(this.patientInfo, "this.patientInfo")
    //   console.log(this.patientAddress, "this.patientAddress")

    // })
  }
  /********************************** Get Staff By Id Function  ******************************************/

  GetStaffById(): void {
    // const id = this.route.snapshot.paramMap.get('id');

    var EmployeeInfo = { "discriminator": this.discriminator, "email": this.employeeEmail }
    this.UserService.GetStaffById(EmployeeInfo)
      .subscribe(staffInfo => {
        this.staffInfo = staffInfo;
        this.FormValidation();
      });
  }
  /********************************** reset Form Function  ******************************************/

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.UserInfo = {
      Discriminator: "",
      UserFirstName: "",
      UserLastName: "",
      UserNickName: "",
      UserGender: "",
      UserDOB: new Date(),
      UserPhone: null,
      UserEmail: "",
      UserImageURL: "",
      IsActive: true,
      Password: "",
      confirmPassword: "",
      role: "",
      isAllowed: true,
      StartDate: new Date(),
      startTime: null,
      endTime: null
    }
    this.UserAddress = {
      Street: "",
      City: "",
      State: "",
      Country: "",
      Postalcode: null
    }
  }
  /********************************** form validation  Function  ******************************************/

  FormValidation() {
    if (this.staffInfo) {
      this.UserInfo.UserEmail = this.staffInfo[0].userEmail,
        this.UserInfo.UserFirstName = this.staffInfo[0].userFirstName,
        this.UserInfo.UserLastName = this.staffInfo[0].userLastName,
        this.UserInfo.UserNickName = this.staffInfo[0].userNickName,
        this.UserInfo.UserGender = this.staffInfo[0].userGender,
        this.UserInfo.UserDOB = new Date(this.staffInfo[0].userDOB),
        this.UserInfo.UserPhone = this.staffInfo[0].userPhone,
        this.UserInfo.role = this.selectedEmployee,
        this.UserAddress.Street = this.staffInfo[0].street,
        this.UserAddress.City = this.staffInfo[0].city,
        this.UserAddress.State = this.staffInfo[0].state,
        this.UserAddress.Country = this.staffInfo[0].country,
        this.UserAddress.Postalcode = this.staffInfo[0].postalcode,
        this.UserInfo.startTime = this.staffInfo[0].userStartWorkingHours,
        this.UserInfo.endTime = this.staffInfo[0].userEndWorkingHours,
        this.UserInfo.StartDate = new Date(this.staffInfo[0].userStartDate),
        this.employeeClicked = false
    }
    else if (this.patientInfo) {
      this.UserInfo.UserEmail = this.patientInfo[0].userEmail,
        this.UserInfo.UserFirstName = this.patientInfo[0].userFirstName,
        this.UserInfo.UserLastName = this.patientInfo[0].userLastName,
        this.UserInfo.UserNickName = this.patientInfo[0].userNickName,
        this.UserInfo.UserGender = this.patientInfo[0].userGender,
        this.UserInfo.UserDOB = new Date(this.patientInfo[0].userDOB),
        this.UserInfo.UserPhone = this.patientInfo[0].userPhone,
        this.UserInfo.role = "Patient",
        this.UserAddress.Street = this.patientAddress[0].street,
        this.UserAddress.City = this.patientAddress[0].city,
        this.UserAddress.State = this.patientAddress[0].state,
        this.UserAddress.Country = this.patientAddress[0].country,
        this.UserAddress.Postalcode = this.patientAddress[0].postalcode,
        this.patientClicked = false
    }
    else {
      this.UserInfo.UserEmail = "",
        this.UserInfo.UserFirstName = "",
        this.UserInfo.UserLastName = "",
        this.UserInfo.UserNickName = "",
        this.UserInfo.UserGender = "",
        this.UserInfo.UserDOB = new Date(),
        this.UserInfo.UserPhone = null,
        this.UserInfo.role = "",
        this.UserAddress.Street = "",
        this.UserAddress.City = "",
        this.UserAddress.State = "",
        this.UserAddress.Country = "",
        this.UserAddress.Postalcode = null,
        this.UserInfo.startTime = null,
        this.UserInfo.endTime = null,
        this.UserInfo.StartDate = new Date(),
        this.UserAddress.Postalcode = null,
        this.UserInfo.startTime = null,
        this.UserInfo.endTime = null,
        this.UserInfo.StartDate = new Date()
    }
    this.staff = true

    this.accountDetailsForm = this.fb.group({
      UserEmail: new FormControl(this.UserInfo.UserEmail, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      ])),
      UserFirstName: new FormControl(this.UserInfo.UserFirstName, Validators.compose([
        Validators.required
      ])),
      // role: new FormControl(this.UserInfo.role, Validators.compose([
      //   Validators.required
      // ])),
      UserLastName: new FormControl(this.UserInfo.UserLastName, Validators.compose([
        Validators.required
      ])),
      // UserNickName: new FormControl(this.UserInfo.UserNickName, Validators.compose([
      //   Validators.required
      // ])),
      // UserGender: new FormControl(this.UserInfo.UserGender, Validators.compose([
      //   Validators.required

      // ])),
      // UserDOB: new FormControl(this.UserInfo.UserDOB, Validators.compose([
      //   Validators.required,
      //   birthDate
      // ])),
      UserPhone: new FormControl(this.UserInfo.UserPhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      // Street: new FormControl(this.UserAddress.Street, Validators.compose([
      //   Validators.required
      // ])),
      // City: new FormControl(this.UserAddress.City, Validators.compose([
      //   Validators.required,
      //   Validators.pattern('^[a-zA-Z]*')
      // ])),
      // State: new FormControl(this.UserAddress.State, Validators.compose([
      //   Validators.required,
      //   Validators.pattern('^[a-zA-Z]*')
      // ])),
      // Country: new FormControl(this.UserAddress.Country, Validators.compose([
      //   Validators.required,
      //   Validators.pattern('^[a-zA-Z]*')
      // ])),
      // Postalcode: new FormControl(this.UserAddress.Postalcode, Validators.compose([
      //   Validators.required,
      //   Validators.pattern('[0-9]{2,5}')
      // ])),
      UserPassword: new FormControl(this.UserInfo.Password, Validators.compose([
        Validators.required,
        Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      ])),
      confirmPassword: new FormControl(this.UserInfo.confirmPassword, Validators.compose([
        Validators.required,
        passwordConfirming])),
      // startTime: new FormControl(this.UserInfo.startTime, Validators.compose([
      //   Validators.required,
      //   checkTime
      // ])),
      // endTime: new FormControl(this.UserInfo.endTime, Validators.compose([
      //   Validators.required,
      //   checkTime
      // ])),
      // StartDate: new FormControl(this.UserInfo.StartDate, Validators.compose([
      //   Validators.required
      // ]))
    })
  }


  /********************************** chooseEmployeeByRole Function  ******************************************/

  chooseEmployeeByRole(choosenRole) {
    console.log(choosenRole)
    if (choosenRole == "Patient") {
      this.Patient = false;
    }
    else {
      this.Patient = true;

    }
  }
  /********************************** OnSubmit Function( register new User)  ******************************************/
  OnSubmit(form: NgForm) {
    console.log(form.value, "signup form")
    form.value['isAllowed'] = true
    this.Employee = false;
    form.value['role'] = "patient"
    this.AuthService.registerUser(form.value)
      .subscribe((data: any) => {

        this.resetForm(form);
        this.toastr.success('User registration successful');
        this.router.navigate(['login'])

        console.log(data, "data")
        // else
        //   this.toastr.error(data.Errors[0]);
      });
    /************************** patient sign up ***********************************/

    // if (!this.role) {
    //   this.Employee = false;
    //   form.value['role'] = "patient"
    //   this.AuthService.registerUser(form.value)
    //     .subscribe((data: any) => {

    //       this.resetForm(form);
    //       this.toastr.success('User registration successful');
    //       this.router.navigate(['login'])

    //       console.log(data, "data")

    //     });
    // }
    /**********************employee edit patient profile*****************************/

    // else if ((this.role.role != "Patient" && this.patientEmail)) {
    //   this.Employee = false;
    //   form.value['role'] = "Patient"
    //   let role = "Patient"
    //   console.log("patient profile ", form.value)
    //   this.PatientService.EditUserProfile(role, form.value)
    //     .subscribe((data: any) => {
    //       this.resetForm(form);
    //       this.toastr.success('Update UserInfo successfully');
    //       this.router.navigate(['patientList'])
    //       console.log(data, "data")

    //     });
    // }

    /**********************employee edit employee profile*****************************/

    // else if (this.role.role != "Patient" && this.AuthService.employeeClicked) {
    //   this.PatientService.EditUserProfile(this.discriminator, form.value)
    //     .subscribe((data: any) => {
    //       this.resetForm(form);
    //       this.toastr.success('Upate UserInfo successfully');
    //       this.router.navigate(['listOfEmployee'])
    //       console.log(data, "data")

    //     });
    // }
    /**********************employee add employee or patient *****************************/

    // else {
    //   this.Employee = true;
    //   console.log(form.value, "true")

    //   this.AuthService.registerUser(form.value)
    //     .subscribe((data: any) => {

    //       this.resetForm(form);
    //       this.toastr.success('User registration successful');
    //       this.router.navigate(['mainPage'])

    //       console.log(data, "data")
    //       // else
    //       //   this.toastr.error(data.Errors[0]);
    //     });
    // }

  }
}
