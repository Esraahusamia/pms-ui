import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgForm, FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';
import { UserInfo } from '../../shared/models/user-info.model';
import { MessageService } from '../../shared/services/message.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { NotifierService } from 'angular-notifier';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  private readonly notifier: NotifierService;

  /********************************** Declear Models & form Groups  ******************************************/
  isLoginError: boolean = false;
  accountDetailsForm: FormGroup;
  UserInfo: UserInfo;
  account_validation_messages: any;
  constructor(notifierService: NotifierService, private AuthService: AuthService, private MessageService: MessageService, private toastr: ToastrService, private fb: FormBuilder, private router: Router) {
    this.notifier = notifierService;
  }

  ngOnInit() {

    this.resetForm();
    this.FormValidation();
    this.account_validation_messages = this.MessageService.account_validation_messages;

  }

  /********************************** reset Form Function  ******************************************/

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.UserInfo = {
      Discriminator: "",
      UserFirstName: "",
      UserLastName: "",
      UserNickName: "",
      UserGender: "",
      UserDOB: new Date(),
      UserPhone: null,
      UserEmail: "",
      UserImageURL: "",
      IsActive: true,
      Password: "",
      confirmPassword: "",
      role: "",
      isAllowed: true,
      StartDate: new Date(),
      startTime: null,
      endTime: null
    }
  }
  /********************************** form validation  Function  ******************************************/

  FormValidation() {
    this.accountDetailsForm = this.fb.group({
      Email: new FormControl(this.UserInfo.UserEmail, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      ])),
      Password: new FormControl(this.UserInfo.Password, Validators.compose([
        Validators.required,
        Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      ]))
    })
  }


  /********************************** OnSubmit Function( login )  ******************************************/

  OnSubmit(form: NgForm) {
    console.log(form.value)
    // this.AuthService.login(form.value).subscribe((data: any) => {
    //   localStorage.setItem('userToken', data.access_token);
    //   this.router.navigate(['/home']);
    // },
    //   (err: HttpErrorResponse) => {
    //     this.isLoginError = true;
    //   });
    form.value['IsAllowedTologin'] = true
    this.AuthService.login(form.value)
      .subscribe((data: any) => {
        this.resetForm(form);

        this.toastr.success('User login successfully');
        this.router.navigate(['home'])
      });
  }
}
