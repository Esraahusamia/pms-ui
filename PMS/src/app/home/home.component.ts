import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userClaims: any;
  role: any;
  Employee: boolean;
  Patient: boolean;
  username: string
  routs: boolean = false;
  mainPageClicked: boolean = false
  email: string
  user: boolean = false;
  constructor(private router: Router, private AuthService: AuthService) { }

  ngOnInit() {
    // this.AuthService.getUserClaims().subscribe((data: any) => {
    //   this.userClaims = data;
    // });
    // this.role = this.AuthService.role
    this.role = JSON.parse(localStorage.getItem("userToken")).role;
    this.username = JSON.parse(localStorage.getItem("userToken")).firstName;
    this.email = JSON.parse(localStorage.getItem("userToken")).email
    console.log(JSON.parse(localStorage.getItem("userToken")), "rooooooole")
    if (!this.role) {
      this.Employee = false;
      this.Patient = false;
    }
    else if (this.role != "patient") {
      this.Patient = false;
      this.Employee = true;
    }
    else if (this.role == "patient") {
      this.Patient = true
      this.Employee = false;
      this.user = true
    }
    else if (this.role != "Admin") {
      this.user = true
    }
    else {
      this.Patient = false;
      this.Employee = false;
    }
  }

  Logout() {
    localStorage.removeItem('userToken');
    this.router.navigate(['/login']);
  }
  Main() {
    // localStorage.removeItem('userToken');
    this.router.navigate(['/signup1']);
  }
  rout() {
    this.routs = true;
    this.router.navigate(['/chart']);
  }
  mainPage() {
    this.AuthService.mainPageClicked = true;
    this.AuthService.employeeClicked = false;
    this.AuthService.patientClicked = false;
    this.AuthService.patientListClicked = false;

  }
  employee() {
    this.AuthService.mainPageClicked = false;
    this.AuthService.employeeClicked = true;
    this.AuthService.patientClicked = false;
    this.AuthService.patientListClicked = false;

  }
  patient() {
    this.router.navigate(['patientProfile', this.email])
    this.AuthService.mainPageClicked = false;
    this.AuthService.employeeClicked = false;
    this.AuthService.patientClicked = true;
    this.AuthService.patientListClicked = false;

  }
  patientList() {
    this.AuthService.mainPageClicked = false;
    this.AuthService.employeeClicked = false;
    this.AuthService.patientClicked = false;
    this.AuthService.patientListClicked = true;
  }
  profile() {
    if (JSON.parse(localStorage.getItem("userToken")).role === "patient") {
      this.router.navigate(['patientProfile', this.email])

    }
    else {
      this.router.navigate(['staffProfile', this.email])

    }
  }
}
