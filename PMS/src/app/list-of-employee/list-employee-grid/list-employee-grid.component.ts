import { Component, OnInit } from '@angular/core';
import { PatientService } from '../../shared/services/patient.service';
import { NgModule, enableProdMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {
  DxSelectBoxModule,
  DxCheckBoxModule,
  DxTextBoxModule,
  DxDateBoxModule,
  DxButtonModule,
  DxValidatorModule,
  DxValidationSummaryModule
} from 'devextreme-angular';

import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list-employee-grid',
  templateUrl: './list-employee-grid.component.html',
  styleUrls: ['./list-employee-grid.component.css']
})
export class ListEmployeeGridComponent implements OnInit {

  /************************************************ Declaration **********************************************/
  Title = [{ "Name": "admin" }, { "Name": "doctor" }, { "Name": "staff" }, { "Name": "assistant" }]

  dataSource

  /************************************************ Constructor **********************************************/

  constructor(private PatientService: PatientService, private router: Router, private toastr: ToastrService) {
    // **************************************** GET ALL Patients  ***************************************//
    this.PatientService.GetAllEmployees().subscribe(res => {
      console.log(res, "all employees")
      this.dataSource = res;
    })

  }

  /************************************************ ngOnInit *********************************************/
  ngOnInit() {
    this.Title
  }

  /***************************************** onRowUpdated function  **************************************/
  onRowUpdated(e) {

    this.PatientService.EditUserProfile(e.key, e.data).subscribe(res => {
      this.toastr.success('User updated successfully');

      console.log(res, "onRowUpdated , response ")
    })
  }

  /***************************************** onRowRemoved function **************************************/
  onRowRemoved(e) {

    this.PatientService.DeactivateUserProfile(e.key).subscribe(res => {
      this.toastr.success('User removed successfully');

      console.log(res, "onRowUpdated , response ")
    })
  }
  /***************************************** Show Profile **************************************/
  onRowAdded(e) {
    console.log(e)
  }
  showProfile(email) {
    this.router.navigate(['staffProfile', email])

  }
  addUser(user) {
    console.log(user, "user")
  }
}
