import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SignUpComponent } from '../user/sign-up/sign-up.component';
import { Directive } from '@angular/core';
import { UserInfo } from '../shared/models/user-info.model';
import { AuthService } from '../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-of-employee',
  templateUrl: './list-of-employee.component.html',
  styleUrls: ['./list-of-employee.component.css']
})

export class ListOfEmployeeComponent implements OnInit {
  UserInfo: UserInfo;



  constructor(private http: HttpClient, private AuthService: AuthService, private router:Router) { }
  signup = true;
  employee = ["Admin", "Doctor", "Staff", "Assistant"];
  emplist;
  selectedEmployee;
  userProfile : boolean;
  userInfoo = true;
  userInfo = {
    city: "",
    country: "",
    postalcode: "",
    state: "",
    street: "",
    userAccount: "",
    userDOB: "",
    userEmail: "",
    userFirstName: "",
    userGender: "",
    userLastName: "",
    userNickName: "",
    userPhone: "",
    userStartDate: "",
    userEndWorkingHours: "",
    userStartWorkingHours: ""
  };

 

  ngOnInit() {
    this.userProfile = false;
  }

  chooseEmployeeByRole(selectedEmployee) {
    this.signup = false;
    this.userProfile = true;
    
    this.router.navigate(['listOfEmployee', selectedEmployee ])
  }

  Edit() {
    this.signup = false;
    this.userProfile = false;
    this.userInfoo = true;
  }

  Deactivate = function (email) {
    window.alert("are you sure you want to deactivate this profile ?");
    this.http.post("http://localhost:53254/NEW/PMS/DeactivateUserAccount/" + email, { "discriminator": this.selectedEmployee })
     .subscribe(res => {
      this.userProfile = true;
      this.userInfoo = true;
    });
  }
}
