import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../shared/services/user.service';
import { PatientService } from '../shared/services/patient.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-staff-profile',
  templateUrl: './staff-profile.component.html',
  styleUrls: ['./staff-profile.component.css']
})
export class StaffProfileComponent implements OnInit {
  /********************************** Declearation ******************************************/

  staffEmail
  employeeInfo
  test: boolean = false

  /********************************** constructor  ******************************************/

  constructor(private route: ActivatedRoute,
    private toastr: ToastrService,
    private UserService: UserService,
    private PatientService: PatientService) { }

  /********************************** ngOnInit  ******************************************/

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(params['staffEmail'])
      this.staffEmail = params['staffEmail']
    })
    var EmployeeInfo = { "discriminator": "doctor", "email": this.staffEmail }

    this.UserService.GetStaffById(EmployeeInfo).subscribe(res => {
      this.employeeInfo = res[0]
      this.test = true
      console.log(res, "GetStaffById")
    })
  }

  /********************************** Deactivate  ******************************************/

  Deactivate(staffEmail) {
    console.log(staffEmail, "staffEmail")
    if (confirm('Are you sure to deactivate this account ?') === true) {
      this.toastr.success('User Deactivated successfully');

      // this.PatientService.DeactivateUserProfile(staffEmail, "doctor").subscribe(res => {
      //   this.toastr.success('User Deactivated successfully');
      // })
    }
  }

}
