import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth.service';
import { MessageService } from '../../shared/services/message.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, NgForm, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PatientService } from '../../shared/services/patient.service';
import { UserService } from '../../shared/services/user.service';
import { UserInfo } from '../../shared/models/user-info.model';
import { UserAddress } from '../../shared/models/user-address.model';

/*********************checkTime (start and end) ***************************/
function checkTime(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const startTime = c.parent.get('startTime');
  const endTime = c.parent.get('endTime')

  if (!startTime || !endTime) return;
  if (startTime.value >= endTime.value) {
    return { invalid: true };

  }
}

/********************* check passwordConfirming ***************************/
function passwordConfirming(c: AbstractControl): any {
  if (!c.parent || !c) return;
  const pwd = c.parent.get('Password');
  const cpwd = c.parent.get('confirmPassword')

  if (!pwd || !cpwd) return;
  if (pwd.value !== cpwd.value) {
    return { invalid: true };

  }
}
@Component({
  selector: 'app-edit-staff-profile',
  templateUrl: './edit-staff-profile.component.html',
  styleUrls: ['./edit-staff-profile.component.css']
})
export class EditStaffProfileComponent implements OnInit {
  /********************************** Declear Models & form Groups  ******************************************/
  roles: any;
  account_validation_messages: any;
  userRegistrationForm: FormGroup;
  accountDetailsForm: FormGroup;
  UserInfo: UserInfo;
  UserAddress: UserAddress;
  staffInfo: any;
  employeeEmail: any;
  staff: boolean = false
  /********************************** get confirm password to check ******************************************/

  get cpwd() {
    return this.accountDetailsForm.get('confirmPassword');
  }
  get endTime() {
    return this.accountDetailsForm.get('endTime');
  }
  /*******************************************constructor*******************************************************/

  constructor(private AuthService: AuthService,
    private MessageService: MessageService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private PatientService: PatientService,
    private UserService: UserService) { }

  ngOnInit() {
    this.employeeEmail = this.route.parent.url.value[0].path
    // console.log(this.route.parent.url.value[0].path, "edit rout params")
    this.roles = ["Admin", "Doctor", "Staff", "Assistant", "Patient"]
    this.account_validation_messages = this.MessageService.account_validation_messages;

    if (this.employeeEmail) {


      this.GetStaffById();

    }
  }

  /********************************** Get Staff By Id Function  ******************************************/
  GetStaffById(): void {

    var EmployeeInfo = { "discriminator": 'doctor', "email": this.employeeEmail }
    this.UserService.GetStaffById(EmployeeInfo)
      .subscribe(staffInfo => {
        console.log(staffInfo, "staaaaaaaaf")
        this.staffInfo = staffInfo;
        this.FormValidation();
      });
  }
  /********************************** reset Form Function  ******************************************/
  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.UserInfo = {
      Discriminator: "",
      UserFirstName: "",
      UserLastName: "",
      UserNickName: "",
      UserGender: "",
      UserDOB: new Date(),
      UserPhone: null,
      UserEmail: "",
      UserImageURL: "",
      IsActive: true,
      Password: "",
      confirmPassword: "",
      role: "",
      isAllowed: true,
      StartDate: new Date(),
      startTime: null,
      endTime: null
    }
    this.UserAddress = {
      Street: "",
      City: "",
      State: "",
      Country: "",
      Postalcode: null
    }
  }
  /********************************** form validation  Function  ******************************************/

  FormValidation() {
    // if (this.staffInfo) {
    //   this.UserInfo.UserEmail = this.staffInfo[0].userEmail,
    //     this.UserInfo.UserFirstName = this.staffInfo[0].userFirstName,
    //     this.UserInfo.UserLastName = this.staffInfo[0].userLastName,
    //     this.UserInfo.UserNickName = this.staffInfo[0].userNickName,
    //     this.UserInfo.UserGender = this.staffInfo[0].userGender,
    //     this.UserInfo.UserDOB = new Date(this.staffInfo[0].userDOB),
    //     this.UserInfo.UserPhone = this.staffInfo[0].userPhone,
    //     // this.UserInfo.role = this.selectedEmployee,
    //     this.UserAddress.Street = this.staffInfo[0].street,
    //     this.UserAddress.City = this.staffInfo[0].city,
    //     this.UserAddress.State = this.staffInfo[0].state,
    //     this.UserAddress.Country = this.staffInfo[0].country,
    //     this.UserAddress.Postalcode = this.staffInfo[0].postalcode,
    //     this.UserInfo.startTime = this.staffInfo[0].userStartWorkingHours,
    //     this.UserInfo.endTime = this.staffInfo[0].userEndWorkingHours,
    //     this.UserInfo.StartDate = new Date(this.staffInfo[0].userStartDate)
    // }

    this.staff = true

    this.accountDetailsForm = this.fb.group({
      UserEmail: new FormControl(this.staffInfo[0].userEmail, Validators.compose([
        Validators.required,
        Validators.pattern("^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$")
      ])),
      UserFirstName: new FormControl(this.staffInfo[0].userFirstName, Validators.compose([
        Validators.required
      ])),
      role: new FormControl(this.staffInfo[0].discriminator, Validators.compose([
        Validators.required
      ])),
      UserLastName: new FormControl(this.staffInfo[0].userLastName, Validators.compose([
        Validators.required
      ])),
      UserNickName: new FormControl(this.staffInfo[0].userNickName, Validators.compose([
        Validators.required
      ])),
      UserGender: new FormControl(this.staffInfo[0].userGender, Validators.compose([
        Validators.required

      ])),
      UserDOB: new FormControl(this.staffInfo[0].userDOB, Validators.compose([
        Validators.required
      ])),
      UserPhone: new FormControl(this.staffInfo[0].userPhone, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{9,9}')
      ])),
      Street: new FormControl(this.staffInfo[0].street, Validators.compose([
        Validators.required
      ])),
      City: new FormControl(this.staffInfo[0].city, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      State: new FormControl(this.staffInfo[0].state, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      Country: new FormControl(this.staffInfo[0].country, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z]*')
      ])),
      Postalcode: new FormControl(this.staffInfo[0].postalcode, Validators.compose([
        Validators.required,
        Validators.pattern('[0-9]{2,5}')
      ])),
      // Password: new FormControl(this.UserInfo.Password, Validators.compose([
      //   Validators.required,
      //   Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
      // ])),
      // confirmPassword: new FormControl(this.UserInfo.confirmPassword, Validators.compose([
      //   Validators.required,
      //   passwordConfirming])),
      // startTime: new FormControl(this.UserInfo.startTime, Validators.compose([
      //   Validators.required,
      //   checkTime
      // ])),
      // endTime: new FormControl(this.UserInfo.endTime, Validators.compose([
      //   Validators.required,
      //   checkTime
      // ])),
      // StartDate: new FormControl(this.UserInfo.StartDate, Validators.compose([
      //   Validators.required
      // ]))
    })
  }

  /********************************** OnSubmit Function( Edit User Info)  ******************************************/
  OnSubmit(form: NgForm) {

    console.log(form.value, "form values")
    /**********************employee edit employee profile*****************************/


    // this.PatientService.EditUserProfile(this.discriminator, form.value)
    //   .subscribe((data: any) => {
    //     this.resetForm(form);
    //     this.toastr.success('Upate UserInfo successfully');
    //     this.router.navigate(['listOfEmployee'])
    //     console.log(data, "data")
    //     // else
    //     //   this.toastr.error(data.Errors[0]);
    //   });


  }
}
