import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../shared/services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-all-employee',
  templateUrl: './all-employee.component.html',
  styleUrls: ['./all-employee.component.css']
})
export class AllEmployeeComponent implements OnInit {

  constructor(private http: HttpClient, private AuthService: AuthService, private route: ActivatedRoute, private router: Router) { }
  emplist;
  selectedEmployee
  x;
  ngOnInit() {

    // const id = this.route.snapshot.paramMap.get('id')
    this.route.params.subscribe(params => {
      this.http.post("http://localhost:53254/new/pms/ListOfEmployees/" + params['id'], "").subscribe(res => {
        this.emplist = res
      });
    })

  }

  // clearList() {
  //   this.x = document.getElementsByName("employee")[0]
  //   for (var i = 0; i < this.x.length; i++) {
  //     this.x[i].selected = 0;
  //   }
  // }
}
