import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-info',
  templateUrl: './employee-info.component.html',
  styleUrls: ['./employee-info.component.css']
})
export class EmployeeInfoComponent implements OnInit {
  flag: boolean;
  email
  id
  constructor(private http: HttpClient, private route: ActivatedRoute) { }
  employeeInfo;


  ngOnInit() {
    const email = this.route.snapshot.paramMap.get('email')
    const id = this.route.snapshot.paramMap.get('id')
    this.route.params.subscribe(params => {
      this.email = params['email']
      this.id = params['id']

    })


    var EmployeeInfo = { "discriminator": this.id, "email": this.email }
    this.http.post("http://localhost:53254/new/pms/GetEmployeeInfo/", EmployeeInfo).subscribe(res => {
      this.employeeInfo = res[0];
      this.flag = true;
    })
  }

}
